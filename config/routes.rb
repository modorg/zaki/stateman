# frozen_string_literal: true
Rails.application.routes.draw do
  resources :organizations

  resources :organizations, only: [] do
    resources :item_types do
      get 'graph_map', controller: 'states'
    end

    resources :item_types, only: [], path: '' do
      match 'summary', to: 'item_types#summary', via: :get
      resources :states
      resources :items, path: ''
    end
  end

  root 'home#index'
  match 'home/private_index' => 'home#private_index', via: :get

  # omniauth
  get '/auth/:provider/callback' => 'user_sessions#create'
  get '/auth/failure' => 'user_sessions#failure'
  get 'auth/autologin' => 'user_sessions#autologin'

  # Custom logout
  match '/logout', to: 'user_sessions#destroy', via: :all
end
