# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'items/new', type: :view do
  let(:organization) { create(:organization) }
  let(:state) { create(:state) }
  let(:item_type) { create(:item_type, organization: organization, initial_state: state) }

  before(:each) do
    assign(:organization, organization)
    assign(:item_type, item_type)
    @item = assign(:item, Item.new(
                            item_type: item_type.reload,
                            state: nil
    ))
  end

  it 'renders new item form' do
    render

    assert_select 'form[action=?][method=?]', organization_item_type_items_path(
      organization, item_type
    ), 'post' do
      assert_select 'input#item_name[name=?]', 'item[name]'
    end
  end
end
