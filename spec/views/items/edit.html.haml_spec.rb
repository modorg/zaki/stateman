# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'items/edit', type: :view do
  let(:organization) { create(:organization) }
  let(:state) { create(:state) }
  let(:item_type) { create(:item_type, organization: organization, initial_state: state) }

  before(:each) do
    @organization = assign(:organization, organization)
    @item_type = assign(:item_type, item_type)
    @item = assign(:item, Item.create!(item_type: item_type, state: state,
                                       name: attributes_for(:item)[:name]))
  end

  it 'renders the edit item form' do
    render

    assert_select 'form[action=?][method=?]', organization_item_type_item_path(
      organization, item_type, @item
    ), 'post' do
      assert_select 'input#item_name[name=?]', 'item[name]'

      assert_select 'select#item_state_id[name=?]', 'item[state_id]'
    end
  end
end
