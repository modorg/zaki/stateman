# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'items/show', type: :view do
  let(:organization) { create(:organization) }
  let(:state) { create(:state) }
  let(:item_type) { create(:item_type, organization: organization, initial_state: state) }

  before(:each) do
    assign(:organization, organization)
    assign(:item_type, item_type)
    @item = assign(:item, Item.create!(item_type: item_type, state: state,
                                       name: attributes_for(:item)[:name]))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(@item.name)
    expect(rendered).to match(@item.item_type.title)
    expect(rendered).to match(@item.state.name)
  end
end
