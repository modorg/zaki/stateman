# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'items/index', type: :view do
  let(:organization) { create(:organization) }
  let(:state) { create(:state) }
  let(:item_type) { create(:item_type, organization: organization, initial_state: state) }

  before(:each) do
    assign(:organization, organization)
    assign(:item_type, item_type)
    @items = assign(:items, [
                      Item.create!(
                        name: 'Name',
                        item_type: item_type.reload,
                        state: state
                      ),
                      Item.create!(
                        name: 'Name1',
                        item_type: item_type.reload,
                        state: state
                      )
                    ])
  end

  it 'renders a list of items' do
    render

    @items.each do |item|
      assert_select 'tr>td', text: item.name
    end
  end
end
