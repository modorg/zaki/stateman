# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'item_types/new', type: :view do
  before(:each) do
    assign(:item_type, build(:item_type))
    @organization = create(:organization)
  end

  it 'renders new item_type form' do
    render

    assert_select 'form[action=?][method=?]', organization_item_types_path(@organization), 'post' do
      assert_select 'input#item_type_title[name=?]', 'item_type[title]'
    end
  end
end
