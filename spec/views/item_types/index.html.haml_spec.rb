# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'item_types/index', type: :view do
  let(:item_type_list) { create_list(:item_type, 2) }

  before(:each) do
    assign(:item_types, item_type_list)
    @organization = create(:organization)
  end

  it 'renders a list of item_types' do
    render
    item_type_list.each do |item_type|
      assert_select 'tr>td', text: item_type.title
    end
  end
end
