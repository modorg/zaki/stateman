# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'item_types/edit', type: :view do
  before(:each) do
    @item_type = assign(:item_type, create(:item_type))
    @organization = assign(:organization, @item_type.organization)
    create(:state, item_type: @item_type)
    @item_type = @item_type.reload
  end

  it 'renders the edit item_type form' do
    render

    assert_select 'form[action=?][method=?]',
                  organization_item_type_path(@organization, @item_type), 'post' do
      assert_select 'input#item_type_title[name=?]', 'item_type[title]'
      assert_select 'select#item_type_initial_state_id[name=?]', 'item_type[initial_state_id]'
    end
  end
end
