# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'item_types/show', type: :view do
  before(:each) do
    @item_type = assign(:item_type, create(:item_type))
    @organization = @item_type.organization
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/Title/)
  end
end
