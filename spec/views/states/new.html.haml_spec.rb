# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'states/new', type: :view do
  let(:organization) { create(:organization) }
  let(:item_type) { create(:item_type, organization: organization) }

  before(:each) do
    assign(:state, build(:state))
    @item_type = assign(:item_type, item_type)
    assign(:organization, organization)
    assign(:states, @item_type.states)
  end

  it 'renders new state form' do
    render

    assert_select 'form[action=?][method=?]', organization_item_type_states_path(organization,
                                                                                 item_type),
                  'post' do

      assert_select 'input#state_name[name=?]', 'state[name]'

      assert_select 'textarea#state_description[name=?]', 'state[description]'

      assert_select 'input#state_color[name=?]', 'state[color]'

      assert_select 'select#state_reachable_state_ids[name=?]', 'state[reachable_state_ids][]'
    end
  end
end
