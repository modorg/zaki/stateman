# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'states/show', type: :view do
  before(:each) do
    @state = assign(:state, create(:state))
    assign(:item_type, create(:item_type))
    assign(:organization, create(:organization))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(@state.name)
    # expect(rendered).to match(@state.description)
    expect(rendered).to match(@state.color)
  end
end
