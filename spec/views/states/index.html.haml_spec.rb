# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'states/index', type: :view do
  let(:organization) { create(:organization) }
  let(:item_type) { create(:item_type, organization: organization) }
  let(:states_list) { create_list(:state, 2, item_type: item_type) }

  before(:each) do
    assign(:states, states_list)
    assign(:organization, organization)
    assign(:item_type, item_type)
  end

  it 'renders a list of states' do
    render

    assert_select 'h2', text: organization.name

    states_list.each do |state|
      assert_select 'tr>td', text: state.name
      assert_select 'tr>td', text: state.description
      assert_select 'tr>td', text: state.color
    end
  end
end
