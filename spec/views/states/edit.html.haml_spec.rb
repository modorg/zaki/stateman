# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'states/edit', type: :view do
  before(:each) do
    @organization = create(:organization)
    @item_type = create(:item_type, organization: @organization)
    @state = assign(:state, create(:state, item_type: @item_type))
    @states = assign(:states, @item_type.states)
  end

  it 'renders the edit state form' do
    render

    assert_select 'form[action=?][method=?]', organization_item_type_state_path(@organization,
                                                                                @item_type, @state),
                  'post' do
      assert_select 'input#state_name[name=?]', 'state[name]'

      assert_select 'textarea#state_description[name=?]', 'state[description]'

      assert_select 'input#state_color[name=?]', 'state[color]'

      assert_select 'select#state_reachable_state_ids[name=?]', 'state[reachable_state_ids][]'
    end
  end
end
