# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'organizations/index', type: :view do
  let(:organization_list) { create_list(:organization, 2) }

  before(:each) do
    assign(:organizations, organization_list)
  end

  it 'renders a list of organizations' do
    render
    organization_list.each do |organization|
      assert_select 'tr>td', text: organization.name
    end
  end
end
