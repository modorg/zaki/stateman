# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ItemType, type: :model do
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:organization) }

  context 'if has states' do
    before { allow(subject).to receive(:states?).and_return(true) }
    it { is_expected.to validate_presence_of(:initial_state) }
  end

  context 'if has no states' do
    before { allow(subject).to receive(:states?).and_return(false) }
    it { is_expected.not_to validate_presence_of(:initial_state) }
  end

  # it { is_expected.to validate_uniqueness_of(:title) }

  describe '#forbid_changing title validation' do
    let(:item_type) { create(:item_type) }

    it 'does not allow changing title' do
      expect(item_type.update(title: 'new title')).to be false
    end
  end

  it { is_expected.to have_many(:states) }
  it { is_expected.to have_many(:items) }
  it { is_expected.to belong_to(:initial_state) }

  describe '#states?' do
    let(:item_type_no_states) { create(:item_type) }
    let(:item_type_with_states) { create(:item_type) }
    before { create(:state, item_type: item_type_with_states) }

    it { expect(item_type_no_states.states?).to be false }
    it { expect(item_type_with_states.reload.states?).to be true }
  end
end
