# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Item, type: :model do
  let(:organization) { create(:organization) }
  let(:state) { create(:state) }
  let(:item_type) { create(:item_type, organization: organization, initial_state: state) }
  subject { Item.new(item_type: item_type, state: state, name: attributes_for(:item)[:name]) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:item_type) }

  # it { is_expected.to validate_presence_of(:state) }
  # since state is set up automatically it doesn't make much sense

  describe '#set_initial_state' do
    let(:item) { Item.new }

    it 'calls set_initial_state' do
      expect(item).to receive(:set_initial_state)
      item.send(:validate)
    end

    it { is_expected.to have_attributes(state: state) }
  end

  # it { is_expected.to validate_uniqueness_of(:name).scoped_to(:item_type) }

  it { is_expected.to belong_to(:item_type) }
  it { is_expected.to belong_to(:state) }
end
