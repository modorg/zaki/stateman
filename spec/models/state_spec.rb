# frozen_string_literal: true
require 'rails_helper'

RSpec.describe State, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:item_type) }
  # it { is_expected.to validate_uniqueness_of(:name).scoped_to(:item_type) }

  describe 'color' do
    it { is_expected.to allow_value('FFFFFF').for(:color) }
    it { is_expected.not_to allow_value('FFFFF').for(:color) }
    it { is_expected.not_to allow_value('FFFFFFF').for(:color) }
  end

  it { is_expected.to belong_to(:item_type) }
  it { is_expected.to have_many(:reachabilities) }
  it { is_expected.to have_many(:reachable_states) }
  it { is_expected.to have_many(:sources) }
  it { is_expected.to have_many(:source_states) }

  describe 'reachabilities' do
    let(:item_type) { create(:item_type) }
    let(:state_closed) { create(:state, item_type: item_type) }
    let(:state_accepted) { create(:state, reachable_states: [state_closed], item_type: item_type) }
    let(:state_rejected) { create(:state, reachable_states: [state_closed], item_type: item_type) }
    let(:state_new) do
      create(:state, reachable_states: [state_accepted, state_rejected],
                     item_type: item_type)
    end

    before { state_new }

    it { expect(state_closed.reachable_states.count).to be_zero }
    it { expect(state_accepted.reachable_states.count).to eq(1) }
    it { expect(state_rejected.reachable_states.count).to eq(1) }
    it { expect(state_new.reachable_states.count).to eq(2) }
    it { expect(state_closed.source_states.count).to eq(2) }
    it { expect(state_accepted.source_states.count).to eq(1) }
    it { expect(state_rejected.source_states.count).to eq(1) }
    it { expect(state_new.source_states.count).to be_zero }

    describe 'reachable state added' do
      before { state_new.reachable_states << state_closed }

      it { expect(state_new.reachable_states.count).to eq(3) }
      it { expect(state_closed.source_states.count).to eq(3) }
    end

    describe 'state_new destroyed' do
      before { state_new.destroy }

      it { expect(state_closed.source_states.count).to eq(2) }
      it { expect(state_accepted.source_states.count).to eq(0) }
      it { expect(state_rejected.source_states.count).to eq(0) }
    end
  end
end
