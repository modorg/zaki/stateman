# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Reachability, type: :model do
  it { is_expected.to validate_presence_of(:destination_state) }
  it { is_expected.to validate_presence_of(:source_state) }

  it { is_expected.to belong_to(:destination_state) }
  it { is_expected.to belong_to(:source_state) }
end
