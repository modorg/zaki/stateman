# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :item do
    sequence :name do |n|
      "item_name#{n}"
    end

    item_type
    state
  end
end
