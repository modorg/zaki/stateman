# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :state do
    sequence :name do |n|
      "state name#{n}"
    end
    association    :item_type
    description    'state description (red)'
    color          'FFFFFF'
  end
end
