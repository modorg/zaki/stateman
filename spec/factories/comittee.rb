# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :organization do
    sequence :name do |n|
      "organization name#{n}"
    end
  end
end
