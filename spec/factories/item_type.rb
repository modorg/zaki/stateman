# frozen_string_literal: true

require 'rails_helper'

FactoryGirl.define do
  factory :item_type do
    sequence :title do |n|
      "item_type_title#{n}"
    end

    association :organization
  end
end
