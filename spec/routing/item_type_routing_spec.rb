# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ItemTypesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: 'organizations/1/item_types').to route_to(
        'item_types#index', organization_id: '1'
      )
    end

    it 'routes to #new' do
      expect(get: 'organizations/1/item_types/new').to route_to(
        'item_types#new', organization_id: '1'
      )
    end

    it 'routes to #show' do
      expect(get: 'organizations/1/item_types/1').to route_to(
        'item_types#show', id: '1', organization_id: '1'
      )
    end

    it 'routes to #edit' do
      expect(get: 'organizations/1/item_types/1/edit').to route_to('item_types#edit',
                                                                   id: '1', organization_id: '1')
    end

    it 'routes to #create' do
      expect(post: 'organizations/1/item_types').to route_to(
        'item_types#create', organization_id: '1'
      )
    end

    it 'routes to #update via PUT' do
      expect(put: 'organizations/1/item_types/1').to route_to(
        'item_types#update', id: '1', organization_id: '1'
      )
    end

    it 'routes to #update via PATCH' do
      expect(patch: 'organizations/1/item_types/1').to route_to(
        'item_types#update', id: '1', organization_id: '1'
      )
    end

    it 'routes to #destroy' do
      expect(delete: 'organizations/1/item_types/1').to route_to(
        'item_types#destroy', id: '1', organization_id: '1'
      )
    end
  end
end
