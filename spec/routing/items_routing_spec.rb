# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ItemsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: 'organizations/1/tasks').to route_to('items#index',
                                                       organization_id: '1',
                                                       item_type_id: 'tasks')
    end

    it 'routes to #new' do
      expect(get: 'organizations/1/tasks/new').to route_to('items#new',
                                                           organization_id: '1',
                                                           item_type_id: 'tasks')
    end

    it 'routes to #show' do
      expect(get: 'organizations/1/tasks/1').to route_to('items#show',
                                                         organization_id: '1',
                                                         item_type_id: 'tasks', id: '1')
    end

    it 'routes to #edit' do
      expect(get: 'organizations/1/tasks/1/edit').to route_to('items#edit',
                                                              organization_id: '1',
                                                              item_type_id: 'tasks', id: '1')
    end

    it 'routes to #create' do
      expect(post: 'organizations/1/tasks').to route_to('items#create',
                                                        organization_id: '1',
                                                        item_type_id: 'tasks')
    end

    it 'routes to #update via PUT' do
      expect(put: 'organizations/1/tasks/1').to route_to('items#update',
                                                         organization_id: '1',
                                                         item_type_id: 'tasks', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: 'organizations/1/tasks/1').to route_to('items#update',
                                                           organization_id: '1',
                                                           item_type_id: 'tasks', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: 'organizations/1/tasks/1').to route_to('items#destroy',
                                                            organization_id: '1',
                                                            item_type_id: 'tasks', id: '1')
    end
  end
end
