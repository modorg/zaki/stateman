# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'States', type: :request do
  describe 'GET /states' do
    let(:organization) { create(:organization) }
    let(:item_type) { create(:item_type, organization: organization) }

    before do
      allow_any_instance_of(ApplicationHelper).to receive(:current_user) do
        create(:user).decorate(is_admin: true)
      end
    end

    it 'works! (now write some real specs)' do
      get organization_item_type_states_path(organization, item_type)
      expect(response).to have_http_status(200)
    end
  end
end
