# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'Organizations', type: :request do
  describe 'GET /organizations' do
    before do
      allow_any_instance_of(ApplicationHelper).to receive(:current_user) do
        create(:user).decorate(is_admin: true)
      end
    end

    it 'works! (now write some real specs)' do
      get organizations_path
      expect(response).to have_http_status(200)
    end
  end
end
