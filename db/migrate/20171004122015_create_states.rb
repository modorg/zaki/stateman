class CreateStates < ActiveRecord::Migration[5.0]
  def change
    create_table :states do |t|
      t.string :name, null: false
      t.belongs_to :item_type, foreign_key: true, null: false
      t.text :description
      t.string :color

      t.timestamps
    end
    add_index :states, [:item_type_id, :name], unique: true
  end
end
