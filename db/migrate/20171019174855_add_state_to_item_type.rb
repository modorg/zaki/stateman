class AddStateToItemType < ActiveRecord::Migration[5.0]
  def change
    add_reference :item_types, :initial_state, references: :states
    add_foreign_key :item_types, :states, column: :initial_state_id
  end
end
