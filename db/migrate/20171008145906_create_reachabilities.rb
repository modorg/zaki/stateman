class CreateReachabilities < ActiveRecord::Migration[5.0]
  def change
    create_table :reachabilities do |t|
      t.references :source_state, references: :states, null: false
      t.references :destination_state, references: :states, null: false

      t.timestamps
    end

    add_foreign_key :reachabilities, :states, column: :source_state_id
    add_foreign_key :reachabilities, :states, column: :destination_state_id

  end
end
