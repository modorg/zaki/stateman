class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.string :name, null: false
      t.belongs_to :item_type, foreign_key: true, null: false
      t.belongs_to :state, foreign_key: true, null: false

      t.timestamps
    end
    add_index :items, [:item_type_id, :name], unique: true
  end
end
