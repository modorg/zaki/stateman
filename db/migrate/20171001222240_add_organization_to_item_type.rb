class AddOrganizationToItemType < ActiveRecord::Migration[5.0]
  def change
    execute 'DELETE FROM item_types;'
    add_reference :item_types, :organization, foreign_key: true, null: false
  end
end
