# frozen_string_literal: true
module ApplicationHelper
  def current_user
    # TODO: secure from admin? called on nil value
    return nil unless session[:user_id]
    @current_user ||= User.find_by(uid: session[:user_id]['uid'])
                          &.decorate(session[:user_id]['extra'])
  end
end
