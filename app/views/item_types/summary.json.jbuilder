# frozen_string_literal: true
json.states @state_items_count do |state, count|
  json.set! :name, state.name
  json.set! :color, state.color
  json.set! :count, count
end
