# frozen_string_literal: true
json.extract! item_type, :id, :title, :organization_id, :created_at, :updated_at
json.url organization_item_type_url(item_type, @organization, format: :json)
