# frozen_string_literal: true
json.extract! state, :id, :name, :item_type_id, :description, :color, :created_at, :updated_at
json.url organization_item_type_state_url(@organization, @item_type, state, format: :json)
