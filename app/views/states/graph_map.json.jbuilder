# frozen_string_literal: true
json.edges do
  json.array! @reachabilities do |reachability|
    json.id "#{reachability.source_state_id}#{reachability.destination_state_id}"
    json.source reachability.source_state_id.to_s
    json.target reachability.destination_state_id.to_s
  end
end

json.nodes @states do |state|
  json.id state.id.to_s
  json.label state.name
  json.color '#' + state.color
  json.size 100
end
