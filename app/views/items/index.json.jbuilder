# frozen_string_literal: true
json.array! @items, partial: 'items/short_item', as: :item
