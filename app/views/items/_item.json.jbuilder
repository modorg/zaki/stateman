# frozen_string_literal: true
json.extract! item, :id, :name, :item_type_id, :state, :reachable_states, :created_at, :updated_at
json.url organization_item_type_item_url(@organization, @item_type, item, format: :json)
