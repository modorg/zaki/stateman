# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

window.draw_state_graph = ->
  # config vars
  data_location = "#{window.location}/graph_map.json"
  container_id = 'state-graph'
  # based on: https://github.com/tweetegy/sigmajs_example
  sigma.parsers.json data_location, { container: container_id }, (s) ->
    #Initialize nodes as a circle
    s.graph.nodes().forEach (node, i, a) ->
      node.x = Math.cos(Math.PI * 2 * i / a.length)
      node.y = Math.sin(Math.PI * 2 * i / a.length) / 4
      return
    s.refresh()
    return
