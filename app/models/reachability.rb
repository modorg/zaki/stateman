# frozen_string_literal: true
class Reachability < ApplicationRecord
  # t.integer  "source_state_id",      null: false
  # t.integer  "destination_state_id", null: false
  # t.datetime "created_at",           null: false
  # t.datetime "updated_at",           null: false
  # t.index ["destination_state_id"],
  # name: "index_reachabilities_on_destination_state_id", using: :btree
  # t.index ["source_state_id"], name: "index_reachabilities_on_source_state_id", using: :btree

  # validations
  validates :destination_state, :source_state, presence: true

  # relations
  belongs_to :destination_state, class_name: 'State'
  belongs_to :source_state, class_name: 'State'
end
