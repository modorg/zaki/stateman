# frozen_string_literal: true
class State < ApplicationRecord
  # t.string   "name",         null: false
  # t.integer  "item_type_id", null: false
  # t.text     "description"
  # t.string   "color"
  # t.datetime "created_at",   null: false
  # t.datetime "updated_at",   null: false
  # t.index ["item_type_id", "name"], name: "index_states_on_item_type_id_and_name",
  # unique: true, using: :btree
  # t.index ["item_type_id"], name: "index_states_on_item_type_id", using: :btree

  # validations
  validates :name, presence: true, uniqueness: { scope: :item_type }
  validates :item_type, presence: true
  validates :color, length: { is: 6 }

  # relations
  belongs_to :item_type
  has_many :reachabilities, foreign_key: 'source_state_id', inverse_of: :source_state,
                            dependent: :destroy
  has_many :reachable_states, through: :reachabilities, source: :destination_state

  has_many :sources, foreign_key: 'destination_state_id', inverse_of: :destination_state,
                     dependent: :destroy, class_name: 'Reachability'
  has_many :source_states, through: :sources, source: :source_state

  has_many :items
end
