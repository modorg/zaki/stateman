# frozen_string_literal: true
class ItemType < ApplicationRecord
  extend FriendlyId
  # t.string   "title",            null: false
  # t.datetime "created_at",       null: false
  # t.datetime "updated_at",       null: false
  # t.integer  "organization_id",  null: false
  # t.integer  "initial_state_id"
  # t.index ["initial_state_id"], name: "index_item_types_on_initial_state_id", using: :btree
  # t.index ["organization_id"], name: "index_item_types_on_organization_id", using: :btree

  # validations
  validates :title, presence: true, uniqueness: { scope: :organization }
  validates :organization, presence: true
  validates :initial_state, presence: true, if: :states?

  validate :forbid_changing_title, on: :update

  # relations
  belongs_to :organization
  has_many :states, dependent: :restrict_with_exception
  has_many :items, dependent: :restrict_with_exception
  belongs_to :initial_state, class_name: 'State'

  # callbacks
  before_destroy :delete_states
  after_create :create_sample_state

  # rest instance methods
  friendly_id :title, use: :slugged
  alias_attribute :slug, :title

  attr_accessor :sample_state_service

  def states?
    states.present?
  end

  private

  def forbid_changing_title
    errors.add(:title, 'can not be changed!') if title_changed?
  end

  def delete_states
    update(initial_state_id: nil)
    states.destroy_all
  end

  def create_sample_state
    State.create(name: 'new', item_type: self, color: '11FF11')

    begin
      service = "#{sample_state_service}Service".constantize
      service.new(organization).perform if defined?(service)
    rescue NameError
      # if it fails here it means that service cannot be found, we just ignore this
      nil
    end
  end
end
