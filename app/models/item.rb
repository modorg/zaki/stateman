# frozen_string_literal: true
class Item < ApplicationRecord
  # t.string   "name",         null: false
  # t.integer  "item_type_id", null: false
  # t.integer  "state_id",     null: false
  # t.datetime "created_at",   null: false
  # t.datetime "updated_at",   null: false
  # t.index ["item_type_id", "name"], name: "index_items_on_item_type_id_and_name",
  # unique: true, using: :btree
  # t.index ["item_type_id"], name: "index_items_on_item_type_id", using: :btree
  # t.index ["state_id"], name: "index_items_on_state_id", using: :btree

  # validations
  validates :name, presence: true, uniqueness: { scope: :item_type }
  validates :item_type, :state, presence: true

  # relations
  belongs_to :item_type
  belongs_to :state

  # callbacks
  before_validation :set_initial_state, on: :create

  delegate :reachable_states, to: :state

  private

  def set_initial_state
    self.state ||= item_type.initial_state || item_type.states.first
  end
end
