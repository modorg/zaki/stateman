# frozen_string_literal: true
class StatesController < ApplicationController
  before_action :set_organization
  before_action :set_item_type
  before_action :set_state, only: [:show, :edit, :update, :destroy]
  before_action :set_states, only: [:edit, :new, :create]

  skip_before_action :authenticate_api, only: :graph_map

  # GET /states
  # GET /states.json
  def index
    @states = @item_type.states.includes(:reachable_states).includes(:source_states).all
  end

  # GET /states/1
  # GET /states/1.json
  def show; end

  # GET /states/new
  def new
    @state = State.new
  end

  # GET /states/1/edit
  def edit; end

  # POST /states
  # POST /states.json
  def create
    @state = State.new(state_params)
    @state.item_type = @item_type

    respond_to do |format|
      if @state.save
        format.html do
          redirect_to [@organization, @item_type, @state], notice: 'State was successfully created.'
        end
        format.json { render :show, status: :created, location: @state }
      else
        format.html { render :new }
        format.json { render json: @state.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /states/1
  # PATCH/PUT /states/1.json
  def update
    respond_to do |format|
      if @state.update(state_params)
        format.html do
          redirect_to [@organization, @item_type, @state],
                      notice: 'State was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @state }
      else
        format.html { render :edit }
        format.json { render json: @state.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /states/1
  # DELETE /states/1.json
  def destroy
    @state.destroy
    respond_to do |format|
      format.html do
        redirect_to [@organization, @item_type, :states],
                    notice: 'State was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  def graph_map
    @states = @item_type.states.order(:id)
    @reachabilities = @states.includes(:reachabilities).collect(&:reachabilities).flatten
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_state
    @state = @item_type.states.find(params[:id])
  end

  def set_states
    @states = @item_type.states.all
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def state_params
    params.require(:state).permit(:name, :item_type_id, :description, :color,
                                  reachable_state_ids: [])
  end
end
