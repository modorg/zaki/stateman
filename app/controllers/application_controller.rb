# frozen_string_literal: true
class ApplicationController < ActionController::Base
  include ApplicationHelper

  protect_from_forgery with: :exception, unless: :json_request?
  before_action :authenticate_api, if: :json_request?
  before_action :admin_required, unless: :json_request?

  rescue_from ActiveRecord::DeleteRestrictionError do |exception|
    redirect_to(:back, alert: exception.message)
  end

  def admin_required
    return if current_user.present? && current_user.admin?
    respond_to do |format|
      format.html do
        redirect_to root_path, alert: 'Access Denied'
      end
      format.json do
        render json: { 'error' => 'Access Denied' }.to_json
      end
    end
  end

  def login_required
    return if current_user
    respond_to do |format|
      format.html do
        redirect_to "/auth/autologin?#{URI.encode_www_form(origin: request.url)}"
      end
      format.json do
        render json: { 'error' => 'Access Denied' }.to_json
      end
    end
  end

  protected

  def set_organization
    @organization = Organization.find(params[:organization_id])
  end

  def set_item_type
    @item_type = @organization.item_types.friendly.find(params[:item_type_id])
  end

  def json_request?
    request.format.json?
  end

  private

  def authenticate_api
    return if request.headers['X-Api-Key'] == Rails.application.secrets.api_key

    head :unauthorized
    false
  end
end
