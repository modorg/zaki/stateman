# frozen_string_literal: true
class ItemsController < ApplicationController
  before_action :set_organization
  before_action :set_item_type
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  # GET /items
  # GET /items.json
  def index
    @items = @item_type.items.all.includes(:state)
  end

  # GET /items/1
  # GET /items/1.json
  def show; end

  # GET /items/new
  def new
    @item = @item_type.items.new
  end

  # GET /items/1/edit
  def edit; end

  # POST /items
  # POST /items.json
  def create
    @item = @item_type.items.new(item_params)

    respond_to do |format|
      if @item.save
        format.html do
          redirect_to [@organization, @item_type, @item],
                      notice: 'Item was successfully created.'
        end
        format.json { render :show, status: :created, location: [@organization, @item_type, @item] }
      else
        format.html { render :new }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html do
          redirect_to [@organization, @item_type, @item],
                      notice: 'Item was successfully updated.'
        end
        format.json { render :show, status: :ok, location: [@organization, @item_type, @item] }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    raise 'Cannot delete item, it can be done anly by api.' unless json_request?
    head :no_content if @item.destroy.save
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_item
    @item = @organization.item_types.friendly.find(params[:item_type_id]).items.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_params
    params.require(:item).permit(:name, :state_id)
  end
end
