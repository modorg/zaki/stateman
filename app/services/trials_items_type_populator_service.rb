# frozen_string_literal: true
class TrialsItemsTypePopulatorService
  def initialize(organization)
    @organization = organization
  end

  def perform
    trials = @organization.item_types.first

    ActiveRecord::Base.transaction do
      zgloszona = trials.states.first

      zgloszona.update!(name: 'zgłoszona', description: 'Próba nowa utworzona przez użytkownika. W trakcie definiowania zadań lub zaraz po zatwierdzeniu przez użytkownika. ', color: '333333')
      zaakceptowana = trials.states.create!(name: 'zaakceptowana - do otwarcia', description: 'Próba zaakceptowana przez KI, do otwarcia przez komendanta.', color: 'DD0000')
      otwarta = trials.states.create!(name: 'otwarta - w trakcie realizacji', description: 'Próba otwarta rozkazem, w trakcie realizacji.', color: '6666FF')
      zamykanapl = trials.states.create!(name: 'zamykana+ (pozytywnie)', description: 'Próba zaakceptowana przez KI do zamknięcia rozkazem przez komendanta.', color: 'DD0000')
      zamknieta = trials.states.create!(name: 'zamknięta (pozytywnie)', description: 'Próba ukończona i zamknięta rozkazem.', color: '003300')
      poprawiana = trials.states.create!(name: 'poprawiana propozycja', description: 'Próba odrzucona przez komisję do poprawy przed otwarciem.', color: 'A52A2A')
      poprawiona = trials.states.create!(name: 'poprawiona', description: 'Poprawiona przez kandydata po odrzuceniu przez komisję, do zaakceptowania.', color: '990000')
      przeterminowana = trials.states.create!(name: 'przeterminowana', description: 'Próbie minął czas realizacji.', color: 'A52A2A')
      otwartaprzed = trials.states.create!(name: 'otwarta - w trakcie realizacji (przedłużona)', description: 'Próba już się przeterminowała ale kandydat zgłosił się na komisję i dostał pozwolenie na kontynuację.', color: '6666FF')
      zakonczona = trials.states.create!(name: 'zakończona (negatywnie)', description: 'Próba zakończona porażką, bez stopnia. ', color: '330000')
      umarzana = trials.states.create!(name: 'umarzana', description: 'Próba która się przeterminowała i ma zostać zamknięta negatywnie przez komendanta.', color: 'DD0000')
      zamykanami = trials.states.create!(name: 'zamykana- (negatywnie)', description: 'Próba do zamknięcia negatywnie przez komendanta.', color: 'DD0000')

      zgloszona.reachable_states = [zaakceptowana, poprawiana]
      zaakceptowana.reachable_states = [otwarta]
      otwarta.reachable_states = [zamykanapl, przeterminowana, zamykanami]
      zamykanapl.reachable_states = [zamknieta]
      # zamknieta.reachable_states = []
      poprawiana.reachable_states = [poprawiona, zakonczona]
      poprawiona.reachable_states = [zaakceptowana, poprawiana, zakonczona]
      przeterminowana.reachable_states = [otwartaprzed, umarzana]
      otwartaprzed.reachable_states = [zamykanapl, przeterminowana, zamykanami]
      # zakonczona.reachable_states = []
      umarzana.reachable_states = [zakonczona]
      zamykanami.reachable_states = [zakonczona]
    end
  end
end
