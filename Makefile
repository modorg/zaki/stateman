make start:
	docker-compose start postgres
	rails s

prepare:
	docker-compose run web rails db:setup

new:
	./bin/rename_templates_variables

